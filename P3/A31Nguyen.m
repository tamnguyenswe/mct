[y2, u2, v2] = read_one_yuv("fg002.yuv", 352, 240); % 352 x 240 pixel
[y1, u1, v1] = read_one_yuv("fg001.yuv", 352, 240);

% ============================================
% Aufgabe 3-1A
diff_y2_y1 = (int16(y2) - int16(y1)) + 128;

diff_entropy = entropy(y2 - y1)

% y1 fig
figure('Name', '3-1A y1');
image(y1);
colormap(gray(256));
daspect([1 1 1]);

% y2 fig
figure('Name', '3-1A y2');
image(y2);
colormap(gray(256));
daspect([1 1 1]);

% diff fig
figure('Name', '3-1A Difference y2 - y1');
image(diff_y2_y1);
colormap(gray(256));
daspect([1 1 1]);


% ============================================
% Aufgabe 3-1B1

[y_max, x_max] = size(y1);

current_frame = y2;
previous_frame = y1;
predicted_frame = zeros(size(y1), 'int16');

BLOCK_SIZE = 8;
range = 15;

y1_mirrored = mirroredframe(y1, range);

% mirrored y1 fig
figure('Name', '3-1B1 y1 Mirrored y1');
image(y1_mirrored);
colormap(gray(256));
daspect([1 1 1]);


% ============================================
% Aufgabe 3-1B2, 3-1B3

% motion vectors for quiver
% rounded before multiplying, this is different from y_max*x_max/BLOCK_SIZE^2
Xs = zeros((y_max / BLOCK_SIZE) * (x_max / BLOCK_SIZE), 1); 
Ys = zeros((y_max / BLOCK_SIZE) * (x_max / BLOCK_SIZE), 1); 
Us = zeros((y_max / BLOCK_SIZE) * (x_max / BLOCK_SIZE), 1); 
Vs = zeros((y_max / BLOCK_SIZE) * (x_max / BLOCK_SIZE), 1); 

index = 0;
for y = 1 : BLOCK_SIZE : y_max
    next_y = y + BLOCK_SIZE - 1;
    y_block = (y - 1) / BLOCK_SIZE;

    for x = 1 : BLOCK_SIZE : x_max
        next_x = x + BLOCK_SIZE - 1;
        x_block = (x - 1) / BLOCK_SIZE;
        index = index + 1;

        [x_offset, y_offset, best_block] = motionestimation(current_frame, previous_frame, range, x_block, y_block);

        predicted_frame(y : next_y, x : next_x) = uint8(best_block); 

        Xs(index) = x;
        Ys(index) = y;
        Us(index) = x_offset;
        Vs(index) = y_offset;
    end
end

diff_y2_predicted = y2 - uint8(predicted_frame) + 128;
diff_entropy = entropy(y2 - uint8(predicted_frame))

% y2 predicted fig
figure('Name', '3-1B3 Predicted y2');
image(predicted_frame);
colormap(gray(256));
daspect([1 1 1]);
hold on;

quiver(Xs, Ys, Us, Vs, 'r');
hold off;

% diff y2 predicted fig
figure('Name', '3-1B3 Difference y2 - predicted');
image(diff_y2_predicted);
colormap(gray(256));
daspect([1 1 1]);


% ============================================
% Aufgabe 3-1C

% flip the timeline
current_frame = y1;
previous_frame = y2;

index = 0;
for y = 1 : BLOCK_SIZE : y_max
    next_y = y + BLOCK_SIZE - 1;
    y_block = (y - 1) / BLOCK_SIZE;

    for x = 1 : BLOCK_SIZE : x_max
        next_x = x + BLOCK_SIZE - 1;
        x_block = (x - 1) / BLOCK_SIZE;
        index = index + 1;

        [x_offset, y_offset, best_block] = motionestimation(current_frame, previous_frame, range, x_block, y_block);

        predicted_frame(y : next_y, x : next_x) = uint8(best_block); 

        Xs(index) = x;
        Ys(index) = y;
        Us(index) = x_offset;
        Vs(index) = y_offset;
    end
end

diff_y1_predicted = y1 - uint8(predicted_frame) + 128;
diff_entropy = entropy(y1 - uint8(predicted_frame))

% y1 predicted fig
figure('Name', '3-1C Predicted y1');
image(predicted_frame);
colormap(gray(256));
daspect([1 1 1]);
hold on;

quiver(Xs, Ys, Us, Vs, 'r');
hold off;

% diff y1 predicted fig
figure('Name', '3-1C Difference y1 - predicted');
image(diff_y1_predicted);
colormap(gray(256));
daspect([1 1 1]);