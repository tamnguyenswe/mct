function [xoffset, yoffset, bestblock] = motionestimation(currentframe, previousframe, searchrange, xblockindex, yblockindex)

mirrored_prev_frame = mirroredframe(previousframe, searchrange);
mirrored_current_frame = mirroredframe(currentframe, searchrange);

[y_max, x_max] = size(mirrored_current_frame);

BLOCK_SIZE = 8;

% reference block coordination in the mirrored img
x_ref_block = xblockindex * BLOCK_SIZE + searchrange + 1;
y_ref_block = yblockindex * BLOCK_SIZE + searchrange + 1;

% best block coordination
x_best_block = 1;
y_best_block = 1;

reference_block = mirrored_current_frame(y_ref_block : y_ref_block + BLOCK_SIZE - 1, x_ref_block : x_ref_block + BLOCK_SIZE - 1);
result = reference_block;
best_mse = 9999999.9; % chose a abitrary large number as the first min value


for y = y_ref_block - searchrange : y_ref_block + searchrange
    next_y = y + BLOCK_SIZE - 1;

    for x = x_ref_block - searchrange : x_ref_block + searchrange
        next_x = x + BLOCK_SIZE - 1;

        this_block = mirrored_prev_frame(y : next_y, x : next_x);
        diff_block = int16(this_block) - int16(reference_block);

        % find the lowest mse
        mse = sum((diff_block).^2, 'all') / (BLOCK_SIZE^2);
        if mse < best_mse
            best_mse = mse;
            x_best_block = x;
            y_best_block = y;
            result = this_block;
        end

    end
end

bestblock = result;
xoffset = x_best_block - x_ref_block;
yoffset = y_best_block - y_ref_block;
