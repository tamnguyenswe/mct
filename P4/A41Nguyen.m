[y1, u1, v1] = read_one_yuv("fg001.yuv", 352, 240); % 352 x 240 pixel
[y2, u2, v2] = read_one_yuv("fg002.yuv", 352, 240); 
[y3, u3, v3] = read_one_yuv("fg003.yuv", 352, 240); 
[y4, u4, v4] = read_one_yuv("fg004.yuv", 352, 240); 
[y5, u5, v5] = read_one_yuv("fg005.yuv", 352, 240); 

% =============================================================
% A4-1 ABCD

[y_max, x_max] = size(y1);
BLOCK_SIZE = 8;
range = 15;

previous_frame = y1;
current_frame = y3;
next_frame = y5;

y3_estimated = zeros(size(y1), 'uint16');
y3_estimated_interpolated = zeros(size(y1), 'uint16');

% Not the required size (this actually has more pixels), but the code will look cleaner this way. 
% The printed out pictures will look the same.
reference = zeros(size(y1), 'uint8'); 
reference_interpolated = zeros(size(y1), 'uint8'); 

for y = 1 : BLOCK_SIZE : y_max
    next_y = y + BLOCK_SIZE - 1;
    y_block = (y - 1) / BLOCK_SIZE;

    for x = 1 : BLOCK_SIZE : x_max
        next_x = x + BLOCK_SIZE - 1;
        x_block = (x - 1) / BLOCK_SIZE;

        [~, ~, best_block_prev, mse_prev] = motionestimation(current_frame, previous_frame, range, x_block, y_block);
        [~, ~, best_block_next, mse_next] = motionestimation(current_frame, next_frame, range, x_block, y_block);

        % choose the block with smaller mse value for 4-1d
        if mse_prev < mse_next
            y3_estimated(y : next_y, x : next_x) = best_block_prev;
            reference(y : next_y, x : next_x) = zeros(BLOCK_SIZE);
        else
            y3_estimated(y : next_y, x : next_x) = best_block_next;
            reference(y : next_y, x : next_x) = ones(BLOCK_SIZE);
        end

        % choose the interpolated block with the smallest mse for 4-1f
        origin_block = current_frame(y : next_y, x : next_x);
        [best_block, best_weight] = get_interpolation(best_block_prev, best_block_next, origin_block);

        y3_estimated_interpolated(y : next_y, x : next_x) = best_block;
        reference_interpolated(y : next_y, x : next_x) = ones(BLOCK_SIZE) * best_weight * 256;

    end
end

diff_y3_predicted = int16(y3) - int16(y3_estimated) + 128;
diff_y3_predicted_interpolated = int16(y3) - int16(y3_estimated_interpolated) + 128;

predicted_entropy = entropy(uint8(y3_estimated))
diff_entropy = entropy(y3 - uint8(y3_estimated)) % = 2,73 < 2,81. Somehow this result is even better than the solution?

predicted_entropy_interpolated = entropy(uint8(y3_estimated_interpolated))
diff_entropy_interpolated = entropy(y3 - uint8(y3_estimated_interpolated))

% y3 predicted fig
figure('Name', '4-1D Predicted y3');
image(y3_estimated);
colormap(gray(256));
daspect([1 1 1]);

% y3 predicted with interpolation fig
figure('Name', '4-1F Predicted y3 with interpolation');
image(y3_estimated_interpolated);
colormap(gray(256));
daspect([1 1 1]);


% diff y3 predicted fig
figure('Name', '4-1D Difference y3 - predicted');
image(diff_y3_predicted);
colormap(gray(256));
daspect([1 1 1]);

% diff y3 predicted fig with interpolation
figure('Name', '4-1F Difference y3 - predicted with interpolation');
image(diff_y3_predicted_interpolated);
colormap(gray(256));
daspect([1 1 1]);


% reference fig
figure('Name', '4-1D Reference, black = prev, white = next');
image(reference);
colormap(gray(2));
daspect([1 1 1]);

% interpolated reference fig
figure('Name', '4-1F Reference with interpolation, dark = prev, light = next');
image(reference_interpolated);
colormap(gray(256));
daspect([1 1 1]);