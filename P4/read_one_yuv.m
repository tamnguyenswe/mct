function [y,u,v] = read_one_yuv(filename, width, height)

%% First, open the video file for binary reading

fid=fopen(filename,'r');
if (fid < 0)
    error('File does not exist!');
end;

y = fread(fid,[width height],'uint8'); % the final dimensions of tmpY are width(rows) x height(columns)
y = uint8(y'); % transposing and casting to uint8 for imshow() to work correctly
u = fread(fid,[width/2 height/2],'uint8');
u = uint8(u'); % transposing and casting to uint8 for imshow() to work correctly
v = fread(fid,[width/2 height/2],'uint8');
v = uint8(v'); % transposing and casting to uint8 for imshow() to work correctly

fclose(fid);