clear all
close all

img_original = imread("lena512.bmp");
img_original = int16(img_original); % Cast to int16

[X_MAX, Y_MAX] = size(img_original);

BLOCK_EDGE = 8;
BLOCK_SIZE = BLOCK_EDGE^2;
IMG_SIZE = numel(img_original);
blocks_amount = IMG_SIZE / BLOCK_SIZE;

q_list = [1 10 20 30 40 50 75 100 200 250 500 1000 10000];

for q = q_list
	img_compressed = img_original; % Make a copy
	counter0 = 0;

	for y = 1 : BLOCK_EDGE : Y_MAX
		next_y = y + BLOCK_EDGE - 1;
		for x = 1 : BLOCK_EDGE : X_MAX
			next_x = x + BLOCK_EDGE - 1;

			dct_domain = dct2(img_compressed(x : next_x, y : next_y));
			quantized_domain = q * round(dct_domain/q);
	        quantized_domain(1,1) = dct_domain(1,1); % Keep DC coeff, only quantize AC coeffs

	        counter0 = counter0 + sum(quantized_domain(:) == 0); % accumulate the number of 0s

			img_compressed(x : next_x, y : next_y) = idct2(quantized_domain);
		end
	end

	q
	k = counter0/blocks_amount % calculate the average number k 

	% just applying a bunch of boring math formulas
	mse = sum((img_original - img_compressed).^2, 'all') / IMG_SIZE;
	psnr = mean(10 * log10((255^2)./mse))

	figure(q); % compressed img.
	image(img_compressed);
	colormap(gray(256));
	daspect([1 1 1]);

	if q == 100
		figure(); % compressed img.
		image(img_compressed(220:300, 220:300));
		colormap(gray(256));
		daspect([1 1 1]);
	end

end
